import Post from "@/models/Post";
import ApiRequest from "../inf/ApiRequest";

export default class PostService{
    static async getList(): Promise<Post[]>{
        const data: any = await ApiRequest.get('https://jsonplaceholder.typicode.com/posts');

        const jsonString: string = JSON.stringify(data.data);

        const posts:Post[] = JSON.parse(jsonString);

        return posts
    }
}